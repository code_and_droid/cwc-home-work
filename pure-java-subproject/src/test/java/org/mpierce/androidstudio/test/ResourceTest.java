package org.mpierce.androidstudio.test;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class ResourceTest {

    @Test
    public void testLoadMainResource() {
        System.out.println(System.getProperty("java.class.path").replace(':', '\n'));
        assertNotNull(getClass().getResourceAsStream("main.txt"));
    }

    @Test
    public void testLoadTestResource() {
        assertNotNull(getClass().getResourceAsStream("test.txt"));
    }
}
